import requests
import base64
import json
import yaml
import io
import datetime
import os
import pytz

current_tz = pytz.timezone('Asia/Shanghai')
ss_prefix = "ss://"
vmess_prefix = "vmess://"
access_token = os.environ['access_token']
jms_url = os.environ['jms_url']
snippets_url = os.environ['snippets_url']

def update_gist_content(content):
    print("start update_gist_content")
    headers = {
        'Content-Type': 'application/json',
        'PRIVATE-TOKEN': access_token,
    }

    data = {"files": [{"content": content, "action": "update","file_path": "gist-proxies.yaml"}]}
    f = io.StringIO(json.dumps(data))
    # f = io.StringIO(string_content)
    # open file snippet.json
    # f = open("snippet.json", "r")
    response = requests.put(snippets_url, headers=headers, data=f)
    print("response: " + str(response))
    print("update_gist_content done")


class JmsNode:
    def __init__(self, name, type, server, port, uuid, password, alterId, cipher, udp):
        self.name = name
        self.type = type
        self.server = server
        self.port = port
        self.uuid = uuid
        self.password = password
        self.alterId = alterId
        self.cipher = cipher
        self.udp = udp

def decode_base64_str(data):
    # return base64.b64decode(data + b'=' * (-len(data) % 4)).decode('utf-8')
    return base64.b64decode(data + b'==').decode('utf-8')

def get_proxies_str():
    resp = requests.get(jms_url)
    resp_decoded = base64.b64decode(resp.text).decode('utf-8')
    resp_decoded_split = resp_decoded.splitlines()

    jms_nodes = []
    for index, mix_node in enumerate(resp_decoded_split):
        index += 1
        print(mix_node + "\n")
        # if ss_prefix in mix_node:
        if mix_node.startswith(ss_prefix):
            print("process ss")
            node_encode_str = mix_node.split("#")[0].replace(ss_prefix, "")
            node_bytes = str.encode(node_encode_str)
            node_decoded = decode_base64_str(node_bytes)
            print(node_decoded)
            # aes-256-gcm:uPpVMAh6bJMf@45.78.10.103:36188, aes-256-gcm is the cipher, uPpZd6bJMf is the password
            # 45.78.10.103 is the server, 36188 is the port
            type = "ss"
            name = type + "-" + str(index)
            cipher = node_decoded.split(":")[0]
            password = node_decoded.split("@")[0].split(":")[-1]
            server = node_decoded.split("@")[1].split(":")[0]
            port = node_decoded.split("@")[1].split(":")[1]
            jms_node = JmsNode(name, type, server, int(port), None,
                            password, None, cipher, None)
            jms_node_dict = jms_node.__dict__
            # remove the null value in the dict
            jms_node_dict = {k: v for k, v in jms_node_dict.items() if v is not None}
            jms_nodes.append(jms_node_dict)

        # if contains vmess://, trim vmess://, then decode
        elif mix_node.startswith(vmess_prefix):
            print("process vmess")
            node_encode_str = mix_node.split("#")[0].replace(vmess_prefix, "")
            node_bytes = str.encode(node_encode_str)
            node_decoded_str = decode_base64_str(node_bytes)
            print(node_decoded_str)
            node_dict = json.loads(node_decoded_str)
            type = "vmess"
            name = type + "-" + str(index)
            if "801" in node_dict["ps"]:
                # get time str 2022-06-19 23:43:51
                name = datetime.datetime.now(current_tz).strftime("%Y-%m-%d %H:%M:%S")
            
            cipher = "auto"
            # {"ps":"JMS-396300@c59s801.jamjams3.net:36188","port":"36188","id":"c601ccee-f254-415b-8d9a-3c596813f443","aid":0,"net":"tcp","type":"none","tls":"none","add":"23.83.227.52"}
            # 36188 is the port, c601ccee-596813f443 is the uuid, aid is the alterId, tcp is the network, add is the server
            server = node_dict["add"]
            port = node_dict["port"]
            uuid = node_dict["id"]
            alterId = node_dict["aid"]
            jms_node = JmsNode(name, type, server, int(port), uuid,
                            None, alterId, cipher, None)
            jms_node_dict = jms_node.__dict__                  
            jms_node_dict = {k: v for k, v in jms_node_dict.items() if v is not None}
            jms_nodes.append(jms_node_dict)
            
    clash_proxiex = {"proxies": jms_nodes}
    return yaml.dump(clash_proxiex)

# to yaml
# with open("clash-proxies.yaml", "w") as f:
#     yaml.dump(clash_proxiex, f)

# add update time string at first line
gist_content = "#update_time: " + str(datetime.datetime.now(current_tz)) + "\n" + get_proxies_str()


update_gist_content(gist_content)
